package com.home_task.saprykin.hometask.views.interfaces;

import com.arellomobile.mvp.MvpView;

/**
 * Created by andrejsaprykin on 08/10/2018.
 */

public interface RepositoryVew extends MvpView{
    void onRepoItemClick(int position);
}
